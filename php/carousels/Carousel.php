﻿<?php
    include('CarouselItem.php');
    require_once('php/dbconnect.php');
    if ($conn) {
        $sql = "SELECT * FROM images WHERE page_id =".$pageId;
        $result = mysqli_query($conn, $sql);
        $carouselsImages = [];
        if ($result) {
            $index= 0;
            while($row = mysqli_fetch_array($result))
            {
                $carouselsImages[$index] = new CarouselItem($row['image_path'], $row['caption']);
                $index ++;
            }
        } else {
            echo  mysqli_error($conn);
        }
    }
?>
    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">

        <!-- indicators -->
        <?php
            $total_carousels = count($carouselsImages);
            echo '<ol class="carousel-indicators">';
            for ($k = 0; $k < $total_carousels; $k++) {
                if ($k == 0) {
                    echo '<li data-target="#myCarousel" data-slide-to="0" class="active"></li>';
                } else {
                    echo '<li data-target="#myCarousel" data-slide-to="' . $k .'"></li>';
                }
            }
            echo '</ol>';
        ?>

        <!-- Wrapper for slides -->
        <?php
        echo '<div class="carousel-inner">';
        if($total_carousels > 0) {
            for ($i = 0; $i < $total_carousels; $i++) {
                $imagePath = $carouselsImages[$i]->getImagePath();
                $caption = $carouselsImages[$i]->getCaption();
                if ($i == 0) {
                    echo '<div class="item active">';
                } else {
                    echo '<div class="item">';
                }
                echo '<div class="fill" style="background-image:url(' . $imagePath . ');"></div>';
                echo '<div class="carousel-caption">';
                echo '<h2>'. $caption .'</h2>';
                echo '</div>';
                echo '</div>';
            }
        }
        echo "</div>";
        ?>


        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>
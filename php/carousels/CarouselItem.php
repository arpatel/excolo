<?php
/**
 * Created by PhpStorm.
 * User: Krupa Patel
 * Date: 11/4/14
 * Time: 7:41 PM
 */

class CarouselItem {

    /* carousel image path*/
    public $imagePath;

    /* carousel image caption*/
    public $caption;

    public function __construct ($imagePath, $caption) {
        $this->imagePath = $imagePath;
        $this->caption = $caption;
    }

    /**
     * @param mixed $caption
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
    }

    /**
     * @return mixed
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * @param mixed $imagePath
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;
    }

    /**
     * @return mixed
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

}


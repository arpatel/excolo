    <?php
        require_once('php/dbconnect.php');
        if ($conn) {
            $companyName = ""; $companyAddress = ""; $companyTelephoneNo = "";
            $companyFaxNo = ""; $companyABN = ""; $companyEmail = "";
            $sql = "SELECT * FROM company_info";
            $result = mysqli_query($conn, $sql);
            if ($result) {
                while($row = mysqli_fetch_array($result))
                {
                    $companyName = $row['company_name'];
                    $companyAddress = $row['company_address'];
                    $companyEmail = $row['company_email'];
                    $companyTelephoneNo = $row['company_telephone'];
                    $companyFaxNo = $row['company_fax'];
                    $companyABN = $row['company_abn'];

                }
            } else {
                echo  mysqli_error($conn);
            }
        }
    ?>

       <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
				
                    <p align="center">
                        <strong> <?php echo $companyName; ?> </strong><br/>
                        <?php echo $companyAddress; ?> <br/>
                        TEL: <?php echo $companyTelephoneNo; ?> &nbsp&nbsp&nbsp FAX: <?php echo $companyFaxNo; ?><br/>
                        <a href="" style="color:#30304e;text-decoration:none;">EMAIL: <?php echo $companyEmail; ?></a><br/>
                        Copyright &copy;<?php echo date('Y')." ". $companyName ?>  ABN: <?php echo $companyABN; ?> , All rights reserved.
					</p>
                </div>
            </div>
        </footer>

    
    <!-- /.container -->

    <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>

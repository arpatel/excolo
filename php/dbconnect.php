<?php
/**
 * Created by PhpStorm.
 * User: Krupa Patel
 * Date: 11/4/14
 * Time: 7:30 PM
 */
$serverName = "localhost";
$userName = "root";
$password = "root";
$dbName = "excolo";

// Create connection
$conn = new mysqli($serverName, $userName, $password, $dbName);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}



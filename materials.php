<?php 
    include('php/include/header.php');
    $pageId = 3;
    include('php/carousels/Carousel.php');
?>


    <!-- Page Content -->
    <div class="container sub">
     <div class="row">
     <div class="col-md-4">
      <img alt="" height="360" src="image/raw-material/material_refinery.jpg" style="margin-left: -75px; margin-top:90px; margin-right: 15px; float: auto;" width="344" class="alignnone">
     </div>
     <div class="col-md-8">
      <div class="page-header">
        <h1>Raw Materials</h1>
      </div> 
          <div id="MaterialKeyText">				          
				<p style=" float: right;"><strong>When</strong> crude oil is refined to fuels and chemicals, help is at hand-in the form of so-called catalysts. Scientists now provide a reference parameter for the performance of an important class of catalysts for petrochemical production.</p>		
          </div>    
            
          <div class="contentLeftUL1">
		  <p style="font-size:16px;">We recycle raw materials that include: petrochemical catalysts-fixed bed catalysts, Alumina based, Zeolite, Resin based, Platinum (Pt), Palladium (Pd), Rhodium (Rh), Molybdenum (Mo), Nickel (Ni), Cobalt (Co), Rhenium (Re); spent auto catalysts-Autocat Converter, decanned spent-catalysts, Platinum (Pt), Palladium (Pd), Rhodium (Rh); carbon catalysts-Purification of Terephthalic Acid (PTA) carbon catalysts, PM Carbon powder catalysts, palladium (Pd), platinum (Pt); Platinum Group Metal (PGM) scrap-dental and jewellery scrap, PGMs from high-purity ingot and bar, Platinum (Pt), Palladium (Pd), Rhodium (Rh), Gold (Au). Some examples of these materials include scrap metal from end of life vehicles (ELV), non-ferrous metals like cans and computer wiring (metals with no iron).</p>
<ul>
    <li>Petro-chemical Catalysts</li>
    <ol>
        <li>Fixed bed Catalysts</li>
        <li> Alumina, Zeolite, Resin based</li>
        <li>Pt, Pd, Rh, Mo, Ni, Co, Re </li>
    </ol>
    <li>Spent Auto Catalysts</li>
    <ol>
  <li>Autocat Converter</li>
  <li>Decanned spent-catalysts</li>
  <li>Pt, Pd, Rh </li>
  </ol>
    <li>Carbon Catalysts</LI>
     <ol>
     <li>PTA carbon catalysts</li>
     <li>PM/ Carbon powder catalysts</li>
     <li>Pd, Pt</li>
     </ol> 
    <li>PGM Scrap</li>
    <ol>
     <li>Dental Scrap, Jewelry Scrap.</li>
     <li>PGMs from high-purity ingot & bar</li>
     <li>Pt, Pd, Rh, Au </li>    
    </ol>
</ul>
</div>
<p style="font-size:16px;">Customer focus is number one for us, which is why we offer a <span style="text-decoration: underline;">simple 3-step solution</span> to sell your scrap metals to us and receive the highest price per pound.</p>

<p><img alt="Excolo" src="image/raw-material/materials1.jpg" style="width: 300px; height: 200px; margin-left: 20px; margin-right: 20px;" width="300" height="200" class="alignnone">
<img alt="Excolo" src="image/raw-material/materials2.jpg" style="width: 300px; height: 200px;" width="300" height="200" class="alignnone"></p>

<p style="font-size:16px;">We specialize in scrap tungsten carbide, high speed tool steel, nickel, molybdenum, cobalt, tantalum, catalyst, metal powder, and thermal spray, recycling these metals from nearly every industry. We offer our customers access to direct markets so that they can maximize the value of their scrap and waste materials.</p>
<p><img alt="Excolo" src="image/raw-material/materials3.jpg" style="width: 300px; height: 200px; margin-left: 20px; margin-right: 20px;" width="300" height="200" class="alignnone">
<img alt="Excolo" src="image/raw-material/materials4.jpg" style="width: 300px; height: 200px;" width="300" height="200" class="alignnone"></p>
   </div>
        

</div>
 
  


        <hr>
</div>

<?php 

 include('php/include/footer.php');

?>
 
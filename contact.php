<?php
    ob_start();
    include('php/include/header.php');
    session_start();

    if($_SERVER['REQUEST_METHOD'] == "POST") {
        $name = trim($_POST['name']);
        $email = trim($_POST['email']);
        $phone = trim($_POST['phone']);
        $comment = trim($_POST['comment']);
        $captcha = trim($_POST['captcha']);

        if(strtolower($captcha) == $_SESSION['captcha']) {

            unset($_SESSION['captcha']);

            // send email
            require_once("php/include/class.phpmailer.php");
            $mail = new PHPMailer();

            $mail->IsSendmail();

            if(!$mail->ValidateAddress($email)) {
                echo "You must specify correct email address.";
                exit;
            } else {
                $email_body = "";
                $email_body = $email_body. "Name : " . $name. "\n";
                $email_body = $email_body. "Phone : " . $phone. "\n";
                $email_body = $email_body. "Comment : " . $comment;

                $mail->SetFrom($email, $name);

                $address = "patelkrupa209@gmail.com";
                $mail->AddAddress($address, "Excolo");

                $mail->Subject = "Excolo contact form submission | ". $name;

                $mail->MsgHTML($email_body);

                if(!$mail->Send()) {
                    echo "Mailer Error: " . $mail->ErrorInfo;
                    exit();
                } else {
                    header('location: contact.php?status=thanks');
                    ob_end_flush();
                    exit;
                }
            }
        } else {
            $captchaError = "You have entered wrong captcha !";
        }

    } else if ($_SERVER['REQUEST_METHOD'] == "GET" && isset($_GET['status'])) {
        echo "Thanks for contacting Excolo. Some one will respond to you as soon as possible";
        include('php/include/footer.php');
        ob_end_flush();
        exit();
    }
?>
<script>
    function validateForm () {
        var name = document.forms['contactForm']['name'].value;
        var email = document.forms['contactForm']['email'].value;
        var phone = document.forms['contactForm']['phone'].value;
        var comment = document.forms['contactForm']['comment'].value;
        var captcha = document.forms['contactForm']['captcha'].value;

        var errorLabel = document.getElementById("error");
        if(name == "") {
            errorLabel.innerHTML = "please enter name";
            return false;
        } else if (email == "") {
            errorLabel.innerHTML = "please enter email";
            return false;
        } else if (phone == "") {
            errorLabel.innerHTML = "please enter phone number";
            return false;
        } else if (isNaN(phone) || phone.length < 9) {
            errorLabel.innerHTML = "please enter valid phone number";
            return false;
        } else if (comment == "") {
            errorLabel.innerHTML = "please enter comment";
            return false;
        } else if (captcha == "") {
            errorLabel.innerHTML = "please enter captcha";
            return false;
        } else if (email != "") {
            var atpos = email.indexOf("@");
            var dotpos = email.lastIndexOf(".");
            if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length) {
                errorLabel.innerHTML = "please enter valid email address";
                return false;
            } else {
                return true;
            }
        }

        return true;
    }

</script>

    <!-- Page Content -->
    <div class="container sub">

        <div class="row">

            <div class="col-md-8">
                <!--<a class="frame" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3151.863253001564!2d144.98827630000002!3d-37.816671899999996!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad642ebfc3b4535%3A0xbfb86a89f812053!2sf8%2F62+Wellington+Parade%2C+East+Melbourne+VIC+3002!5e0!3m2!1sen!2sau!4v1414274091961" target="_blank"></a>-->
                <iframe class="frame" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3151.863253001564!2d144.98827630000002!3d-37.816671899999996!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad642ebfc3b4535%3A0xbfb86a89f812053!2sf8%2F62+Wellington+Parade%2C+East+Melbourne+VIC+3002!5e0!3m2!1sen!2sau!4v1414274091961" width="600" height="550" frameborder="10px" style="border:3px solid green"></iframe>
            </div>

            <div class="col-md-4">

                <div class="page-header">
                    <h1>Contact Us</h1>
                </div>

                <?php
                    if(isset($captchaError)) {
                        renderForm ($name, $email, $phone, $comment, $captchaError);
                    } else {
                        renderForm('', '', '', '', '');
                    }
                ?>

                <script>
                    $(function () {

                        $('#reload').click(function () {
                            $('#captchaimg').attr('src', 'php/include/captcha.php?' + (new Date).getTime());
                            return false;
                        });
                    });
                </script>
            </div>
        </div>

        <hr/>

    </div>

<?php
    function renderForm ($name, $email, $phone, $comment, $error) { ?>

        <form name="contactForm" action="contact.php" method="post" onsubmit="return validateForm()">

            <div class="form-group" id="error-div">
                <label id="error" style="color: red"> <?php echo $error; ?> </label>
            </div>

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="<?php echo $name; ?>">
            </div>

            <div class="form-group">
                <label for="email">Email address</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Enter email" value="<?php echo $email; ?>">
            </div>

            <div class="form-group">
                <label for="phone">phone</label>
                <input type="text" class="form-control" name="phone" id="phone" placeholder="Enter phone" value="<?php echo $phone; ?>">
            </div>

            <div class="form-group">
                <label for="comment">Comment</label>
                <textarea rows="4" class="form-control" name="comment" id="comment" placeholder="Enter Comment"> <?php echo $comment; ?> </textarea>
            </div>
            <div class="form-group">
                <label for="captcha">Fill captcha</label>

                <div>
                    <span><input type="text" class="form-control" name="captcha" id="captcha" required="" /></span>
                    <span><img style="margin-top:5px" src="php/include/captcha.php" id="captchaimg" /> </span>

                    <span id="reload" style="cursor: pointer;"><img src="image/contact-us/refresh.png"  width="16" height="15"  ></span>
                </div>
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    <?php } ?>

<?php
    include('php/include/footer.php');
    ob_end_flush();
?>
 
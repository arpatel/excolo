<?php 
    include('php/include/header.php');
    $pageId =5;
    include('php/carousels/Carousel.php');
?>


    <!-- Page Content -->
    <div class="container sub">
     <div class="row">
     <div class="col-md-4">
      <img alt="" height="360" src="image/catalyst/catalyst_refinery.jpg" style="margin-left: -75px; margin-top:90px; margin-right: 15px; float: auto;" width="344" class="alignnone">
     </div>
     <div class="col-md-8">
      <div class="page-header">
        <h1>Catalysts</h1>
      </div> 
          <div id="MaterialKeyText">				          
				<p style=" float: right;"><strong>A catalyst</strong>  accelerates the rate of a reaction by interacting with reactants and products. The catalyst is not consumed during the reaction. The correct catalyst is also highly selective by favouring the desired product over undesirable ones. Catalysts play a crucial role in 90% of all commercially produced chemical products.</p>		
          </div>    
            
          <div class="contentLeftUL1">
          
<img alt="Excolo" src="image/catalyst/catalyst.jpg" width="95%" height="95%" class="alignnone">
<hr/>
<div> <strong>Material : Spent Catalysts of EG/EO process </strong> </div>
<hr/>
<img alt="Excolo" src="image/catalyst/catalystprocess.jpg" width="95%" height="95%" class="alignnone">

</div>
<p>Customer focus is number one for us, which is why we offer a <span style="text-decoration: underline;">simple 3-step solution</span> to sell your scrap metals to us and receive the highest price per pound.</p>

<p><img alt="Excolo" src="image/catalyst/catalyst3.png" style="width: 300px; height: 200px; margin-left: 20px; margin-right: 20px;" width="300" height="200" class="alignnone">
<img alt="Excolo" src="image/catalyst/catalyst4.png" style="width: 300px; height: 200px;" width="300" height="200" class="alignnone"></p>

<p>We specialize in scrap tungsten carbide, high speed tool steel, nickel, molybdenum, cobalt, tantalum, catalyst, metal powder, and thermal spray, recycling these metals from nearly every industry. We offer our customers access to direct markets so that they can maximize the value of their scrap and waste materials.</p>
<p><img alt="Excolo" src="image/catalyst/catalyst1.jpg" style="width: 300px; height: 200px; margin-left: 20px; margin-right: 20px;" width="300" height="200" class="alignnone">
<img alt="Excolo" src="image/catalyst/catalyst2.jpg" style="width: 300px; height: 200px;" width="300" height="200" class="alignnone"></p>
   </div>
        

</div>
 
  


        <hr>
</div>

<?php 

 include('php/include/footer.php');

?>
 
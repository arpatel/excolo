<?php 
    include('php/include/header.php');
   
?>


<!-- Page Content -->
    <div class="container">
        <!-- Intro Content -->
        <div class="row">
            <div class="col-md-4" style="margin-top:75px;">
                <img class="img-responsive" src="image/about/about_company.png" alt="">
            </div>
            <div class="col-md-8">
                <h2 style="border-bottom:2px solid #abc42b;padding-bottom:15px;">About Excolo Metals</h2>
                <p class="mainpara">We recycle catalyst waste including petrochemical, fertilizer, oil, hydrogenation, adhesives, food and pharmaceutical Industry. It contains precious metals and Platinum Group Metals(PGMs). We think clean the environment and conserve natural resources.</p>
                <div class="parul">
                <p>An Australian based company in spent petrochemical catalyst recycling. We are one of the leading exporters of spent auto catalysts in Australia. We are also an exclusive overseas supplier to the <a href=http://www.hspmtech.com>Heesing PMTech Corp.</a> refinery in Korea.</p>
				<p>Founded in February 2014 from our parent company-HRM Chemicals Pty Ltd. All about Chemical Trading, our business runs in a global scale, networking with Korea, Japan, Hong Kong, Bangkok, New Zealand, and all over Australia.</p>
                <p>Excolo Metals recover and refine a wide range of proprietary base PGM, precious and non-ferrous metal catalyst. We purchase all catalysts from petrochemical, fertilizer, oil, hydrogenation, adhesives, food and pharmaceutical Industry on a worldwide basis.                </p>
                <p>Together, with our co-operative company - <a href=”http://www.cicmaster.com.au”>CIC Masters Pty Ltd</a>, we look towards a clean environment and work towards conserving these natural resources.</p>
				</div>
            </div>
        </div>
       
	<!-- Global Network -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Our Network</h2>
            </div>
            <div class="col-md-16 text-center">
                <div class="thumbnail">
                    <img class="img-responsive" src="image/about/Excolometals global_network.png" alt="">
                    <div class="caption">
                        <h3>Excolo Metals <br>
                            <small>Global Network</small>
                        </h3>
                        <p>Our business runs in a global scale, networking with Korea, Japan, Hong Kong, Bangkok, New Zealand, and all over Australia.</p>
                    </div>
                </div>
            </div>
 
		</div>
		<!-- /.row -->
	</div>
    <!-- /.container -->
	
	
	<?php 

 include('php/include/footer.php');

?>
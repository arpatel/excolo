<?php 
    include('php/include/header.php');
    $pageId = 4;
    include('php/carousels/Carousel.php');
?>


    <!-- Page Content -->
    <div class="container sub">
     <div class="row">
     <div class="col-md-4">
      <img alt="" height="360" src="image/recycling/recycling_refinery.jpg" style="margin-left: -75px; margin-top:90px; margin-right: 15px; float: auto;" width="344" class="alignnone">
     </div>
     <div class="col-md-8">
      <div class="page-header">
        <h1>Recycling</h1>
      </div> 
          <div id="MaterialKeyText">				          
				<p style=" float: right;"><strong>Reducing</strong> landfill and conserve natural resources of the Earth  with our recycling processes.</p>		
          </div>    
            <p>We  recover and refine spent catalysts of PGMs with a high purification of 99.95%. So, we provide the best prices for petrochemical catalyst conversion, a good rate for the changing market value. We have a fast turnaround, and guarantee good service and same day payment. We also offer collection services to our customers.</p>
          <div class="contentLeftUL1">
<img alt="Excolo" src="./image/recycling/recyclingprocess.jpg" width="95%" height="95%" class="alignnone">
</div>
<p>Customer focus is number one for us, which is why we offer a <span style="text-decoration: underline;">simple 3-step solution</span> to sell your scrap metals to us and receive the highest price per pound.</p>

<p><img alt="Excolo" src="./image/recycling/recycling1.jpg" style="width: 300px; height: 200px; margin-left: 20px; margin-right: 20px;" width="300" height="200" class="alignnone">
<img alt="Excolo" src="./image/recycling/recycling2.jpg" style="width: 300px; height: 200px;" width="300" height="200" class="alignnone"></p>

<p>We specialize in scrap tungsten carbide, high speed tool steel, nickel, molybdenum, cobalt, tantalum, catalyst, metal powder, and thermal spray, recycling these metals from nearly every industry. We offer our customers access to direct markets so that they can maximize the value of their scrap and waste materials.</p>
<p><img alt="Excolo" src="./image/recycling/recycling3.jpg" style="width: 300px; height: 200px; margin-left: 20px; margin-right: 20px;" width="300" height="200" class="alignnone">
<img alt="Excolo" src="./image/recycling/recycling4.jpg" style="width: 300px; height: 200px;" width="300" height="200" class="alignnone"></p>
   </div>
        

</div>

        <hr>
</div>

<?php 

 include('php/include/footer.php');

?>
 
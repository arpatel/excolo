﻿<?php 
    include('php/include/header.php');
    $pageId = 1;
    include('php/carousels/Carousel.php');
?>
    <!-- Page Content -->
    <div class="container sub">

        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Welcome to Excolo Metals
                </h1>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-check"></i>About Excolo Metals </h4>
                    </div>
                    <div class="panel-body">
                        <p>To Reduce Global Warming. To Prevent PollutionTo Save Energy. To maintain the Nature. We recycle all metals.</p>
                       <p> We recover and refine spent catalysts of PGMs with a high purification of 99.95%. So, we provide the best prices for petrochemical catalyst conversion, a good rate for the changing market value. We have a fast turnaround, and guarantee good service and same day payment.Founded in February 2014 from our parent company – ‘HRM Chemicals Pty Ltd.</p>
                        <a href="#" class="btn btn-default">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-recycle"></i>Why should we Recycle</h4>
                    </div>
                    <div class="panel-body">
                        <p>To Reduce Global Warming. To Prevent PollutionTo Save Energy. To maintain the Nature. We recycle all metals.</p>
                       <p> We recover and refine spent catalysts of PGMs with a high purification of 99.95%.So,we provide the best prices for petrochemical catalyst conversion,a good rate for the changing market value.We have a fast turnaround, and guarantee good service and same day payment. We also offer collection services to our customers.</p>
					   <p> </p>
                        <a href="#" class="btn btn-default">Learn More</a>
                    </div>
                </div>
            </div>
			 <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-dollar"></i>Current Metals Prices</h4>
                    </div>
                    <div class="panel-body-price">
                        <a href='http://www.fastmarkets.com/pricelink'> <img border=0 src='http://www.fastmarkets.com/freecharts/freeprice.aspx?id=f8706daa-a636-4b90-9e32-a85d3935f77e'></a>
                        
                    </div>
                </div>
            </div>
        </div>

        <hr>
        
</div>

<?php 

 include('php/include/footer.php');

?>
 